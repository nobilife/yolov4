import os
import cv2
from tqdm import tqdm
from multiprocessing import Process, Pool 
from glob import glob

def split_(image):
    name = os.path.basename(image)
    data = name.split(".")
    if os.path.exists(os.path.join(os.getcwd() ,"raw_image/")+ data[0]+ "_0.jpg"):
        return
    else:
        image = cv2.imread(image)
        H, W, C = image.shape
        if W == "2720":
            image = image[:, 160:]
        images = []
        images.append(image[:H//2, :W//2, :])
        images.append(image[:H//2, W//2:, :])
        images.append(image[H//2:, :W//2, :])
        images.append(image[H//2:, W//2:, :])
        for ii, img in enumerate(images):
            cv2.imwrite(os.path.join(os.getcwd() ,"splitted_img/") + data[0] + "_"+"%d.jpg"%ii, img)\

if __name__=="__main__":
    with Pool(4) as p:
        p.map(split_, tqdm(glob(os.path.join(os.getcwd(),"raw_image/")+"/*.jpg")))
  