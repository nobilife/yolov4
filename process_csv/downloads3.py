import boto3
import os
import pandas as pd
from tqdm import tqdm
from multiprocessing import Pool
from glob import glob
import re
import argparse

parser = argparse.ArgumentParser(description='Process trainning data demo')
parser.add_argument('--incsv', type=str, required=True,
                    help='the input CSV data document')
parser.add_argument('--type', type=str, required=True,help='the input CSV data document') # bed or person
args = parser.parse_args()
print(args.incsv)


def manage_folder():
    if not os.path.exists(os.path.join(os.getcwd(),"raw_image")):
        os.makedirs(os.path.join(os.getcwd(),"raw_image"))

    if not os.path.exists(os.path.join(os.getcwd(),"splitted_img")):
        os.makedirs(os.path.join(os.getcwd(),"splitted_img"))

    if not os.path.exists(os.path.join(os.getcwd(),"train_image")):
        os.makedirs(os.path.join(os.getcwd(),"train_image"))  
        

def func(image):
    baddsename = os.path.basename(image)
    # id, img_camera_id, extension = re.findall("(.*)([0-3])(.jpg)", baddsename)[0]
    # filename = id + extension
    filename = image
    if not ".jpg" in filename:
        filename = filename + ".jpg"
    if not os.path.exists(os.path.join(os.getcwd(),"raw_image/")+f"{filename}"):
        print(name, "image is not exist, can be download")
        try:
            s3.download_file("nobi-production", f"{filename}", os.path.join(os.getcwd(),"raw_image") + f"{filename}") # .jpg
        except Exception as e:
            print(e, filename)
    else:
        print(name, "existed")
        # pass


if __name__ == '__main__':
    manage_folder()
    s3 = boto3.client("s3",
                      aws_access_key_id="AKIASPJHJVJVCHUS65PR",
                      aws_secret_access_key="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")
    df = pd.read_csv(args.incsv)
    if (args.type == "bed"):
        name = df.loc[:, "image_key"].drop_duplicates()
    elif (args.type == "person"):
        name = df.loc[:, "image_id"].drop_duplicates()
    
    with Pool(4) as p:
        p.map(func, name)

   
