import os 
from tqdm import tqdm 
from glob import glob 
import shutil
import argparse
from multiprocessing import Pool

parser = argparse.ArgumentParser(description='Process trainning data demo')
parser.add_argument('--src', type=str, required=True,
                    help='the source folder contain the txt of the image')

args = parser.parse_args()
print(args.src)

'''This func used to change the label of image for training YoloV4 detection, '''
def modify_label(label):
    # import ipdb; ipdb.set_trace()
    with open(label, "r") as f:
        lines = f.read().strip().splitlines()
    a = 0
    b =0
    keep_label = []
    for line in lines:

        if line.startswith("3"):
            a +=1
        if line.startswith("5"):
            b +=1
    if a > 0 and b > 0:
        for line1 in lines:
            
                if line1.startswith("5"):
                    temp = line1.replace(line1[0], "6")
                    keep_label.append(temp)
                if not line1.startswith("5"):
                    keep_label.append(line1)
        with open(label, "w") as f:
            f.write("\n".join(keep_label))


if __name__ == "__main__":
    for label in glob(args.src+"/*txt"):
        modify_label(label)