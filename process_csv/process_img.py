import os
from posixpath import join 
import shutil
from sys import path
from ipdb.__main__ import launch_ipdb_on_exception
from numpy.lib.function_base import append
from numpy.lib.utils import source
from pandas.core.indexes import base 
from tqdm import tqdm
from multiprocessing import Pool
from glob import glob
import pandas as pd
import cv2
import numpy as np
import re
import argparse
import boto3
import time
from threading import Thread
cls_dict = {"standing": 0, "sitting": 1, "sitting_on_bed": 2, "lying_on_bed": 3,
            "lying_on_floor": 4, "no_label": 7, "OCCUPIED_BED": 5, "EMPTY_BED": 6}
parser = argparse.ArgumentParser(description='Process trainning data demo')
parser.add_argument('--incsv', type=str, required=True,
                    help='the input CSV data document')
parser.add_argument('--type', type=str, required=True,
help='the input CSV data document')
args = parser.parse_args()
print(args.incsv)

def bbox_beds(min_x, min_y, max_x, max_y, img_width, img_height):
    x2 = max_x
    x1 = min_x
    y2 = max_y
    y1 = min_y
    dw = float(1/img_width)
    dh = float(1/img_height)
    d = float((x2 - x1))
    d1 = float((y2 - y1))

    xc = float((x1+x2)/2)*dw
    yc = float((y1+y2)/2)*dh
    w = float((x2 - x1)*dw)
    h = float((y2 - y1)*dh)
    
    return (xc,yc,w,h) 

def bbox_cal(x,y, img_width, img_height):
    x = x[1:len(x)-1].split(",")
    y = y[1:len(y)-1].split(",")
    for i in range(len(x)):
        if "." in x[i]:
            x[i] = x[i].replace(".", "")
    for j in range(len(x)):
        if "." in y[j]:
            y[j] = y[j].replace(".", "") 
    x = list(map(int, x))
    y = list(map(int, y))
    # import ipdb; ipdb.set_trace()
    x2 = float(max(x))
    x1 = float(min(x))
    y2 = float(max(y))
    y1 = float(min(y))

    dw = float(1/img_width)
    dh = float(1/img_height)
    d = float((x2 - x1))
    d1 = float((y2 - y1))
    s = d*d1
    if s > 70000:
        r=1.3
    elif s <= 70000:
        r=1.2

    xc = float((x1+x2)/2)*dw
    yc = float((y1+y2)/2)*dh
    w = float((x2 - x1)*dw)*r
    h = float((y2 - y1)*dh)*r
    
    return (xc,yc,w,h)  

data = pd.read_csv(args.incsv)
data.fillna('', inplace=True)

def analysis_csv(image):
    # "device_capture_id","device_id","resident_id","taken_on","camera","room_type","pose","invalid_human_detection","image_id","x","y","confidence","device_capture_link","pose_link"
    img = cv2.imread(image)
    img_width = img.shape[1]
    img_height = img.shape[0]
    baddsename = os.path.basename(image)
    key_ = baddsename.split("_")
    df = data.loc[data.loc[:, "image_id"]=="".join([key_[0], ".jpg"]), ["camera","pose","image_id","x","y"]]
    if len(df)==0:
        # import ipdb; ipdb.set_trace()
        return
    for idx, dd in df.iterrows():
        if dd["camera"] != "":
            try:
                if int(dd["camera"]) == int(key_[1][:-4]):
                    if dd["pose"] == "":
                        if len(df) == 1:
                            with open(image.replace(".jpg", ".txt"), "w") as f:
                                f.write("")
                        else:
                            continue
                    if dd["pose"] != "":
                        with open(image.replace(".jpg", ".txt"), "w") as f:
                            label = []
                            # proces undified classes
                            if dd["pose"] == "lying_under_covers":
                                dd["pose"] = "lying_on_bed"
                            cls = dd["pose"]
                            # if cls == "":
                            #     cls = "no_label"
                            camera_id = dd["camera"]
                            x = dd["x"]
                            y = dd["y"]
                            # import ipdb; ipdb.set_trace()
                            xc, yc, w, h = bbox_cal(x,y, img_width, img_height)
                            label.append(" ".join([str(cls_dict[cls]), str(xc), str(yc), str(w), str(h)]))
                            f.write("\n".join(label))
                            label = []
            except:
                import ipdb; ipdb.set_trace()

def check_acceptable(image):
    if os.path.exists(image) and os.path.exists(image.replace(".jpg", ".txt")):
        if args.type =="person":
            shutil.move(image, "train_Person")
            shutil.move(image.replace(".jpg", ".txt"), "train_Person")
        if args.type =="bed":
            shutil.move(image, "train_BED")
            shutil.move(image.replace(".jpg", ".txt"), "train_BED")

def analysis_bedcsv(image):
    # "name","min_x","min_y","max_x","max_y","camera","image_key"
    img = cv2.imread(image)
    img_width = img.shape[1]
    img_height = img.shape[0]
    # print(mage)
    baddsename = os.path.basename(image)
    key_ = baddsename.split("_")
    df = data.loc[data.loc[:, "image_key"]=="".join([key_[0], ".jpg"]), ["name","min_x","min_y","max_x","max_y","camera","image_key"]]
    if len(df)==0:
        # import ipdb; ipdb.set_trace()
        return
    for idx, dd in df.iterrows():
        try:
            if int(dd["camera"]) == int(key_[1][:-4]):
                with open(image.replace(".jpg", ".txt"), "w") as f:
                    label = []
                # import ipdb; ipdb.set_trace()
                    cls = dd["name"]
                    camera_id = dd["camera"]
                    min_x = dd["min_x"]
                    min_y = dd["min_y"]
                    max_x = dd["max_x"]
                    max_y = dd["max_y"]
                    # import ipdb; ipdb.set_trace()
                    xc, yc, w, h = bbox_beds(min_x, min_y, max_x, max_y, img_width, img_height)
                    label.append(" ".join([str(cls_dict[cls]), str(xc), str(yc), str(w), str(h)]))
                    f.write("\n".join(label))
                    label = []
        except:
            import ipdb; ipdb.set_trace()

if __name__=="__main__":
    if args.type =="bed":
        # os.path.join(os.getcwd(), "splitted_BED/")+"*.jpg"
        with Pool(32) as p:
            p.map(analysis_bedcsv, tqdm(glob(os.path.join(os.getcwd(), "splitted_BED/")+"*.jpg")))
        with Pool(32) as p:
            p.map(check_acceptable, tqdm(glob(os.path.join(os.getcwd(), "splitted_BED/")+"*.jpg")))
    if args.type == "person":
        with Pool(32) as p:
            p.map(analysis_csv, tqdm(glob(os.path.join(os.getcwd(), "splitted_Person/")+"*.jpg")))
        with Pool(32) as p:
            p.map(check_acceptable, tqdm(glob(os.path.join(os.getcwd(), "splitted_Person/")+"*.jpg")))

    