from glob import glob
from multiprocessing import Process, Pool
from time import time
import cv2
import base64
import requests
import numpy as np
import os

route = "/yolov4/inference"
URL = "http://{}:{}{}".format("127.0.0.1", "2210", route)

idx_cls_dict = {0: "standing", 1: "sitting", 2: "sitting on bed", 3: "lying on bed",
                4: "lying on floor", 7: "no label", 5: "occupied_bed", 6: "empty_bed"}
cls_dict = {"standing": 0, "sitting": 1, "sitting on bed": 2, "lying on bed": 3,
            "lying on floor": 4, "no label": 7, "occupied_bed": 5, "empty_bed": 6}

def draw(img, bboxes,color):
    if color ==1:
        for bb in bboxes:
            cv2.rectangle(img, (bb["x1"], bb["y1"]), (bb["x2"], bb["y2"]), (0, 255, 0), 1)
            cv2.putText(img, str(
                idx_cls_dict[bb["cls"]] +"_real"), (bb["x1"], bb["y1"]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1, cv2.LINE_AA)
    if color ==2:
        for bb in bboxes:
            cv2.rectangle(img, (bb["x1"], bb["y1"]), (bb["x2"], bb["y2"]), (0,0,255), 1)
            cv2.putText(img, str(
                idx_cls_dict[bb["cls"]] +" "+str(bb["pp"])), (bb["x1"], bb["y1"]+20), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,255), 2, cv2.LINE_AA)

    return img

def request_micro_service(image_path):
    #print(image_path)
    image = cv2.imread(image_path)

    _, np_img = cv2.imencode(".png", image)
    byte_img = np_img.tostring()
    base64_bytes = base64.b64encode(byte_img)
    response = requests.post(url=URL, data=base64_bytes, headers={
        'content-type': 'image/png'})

    # print(response.ok)
    if response.ok:
        r = response.json()
    else:
        return
    keep_labels = []
    import ipdb;ipdb.set_trace()

    with open(image_path.replace(".jpg", ".txt"), mode="r", encoding="utf-8") as f:
        lines = f.read().strip().splitlines()
    for line in lines:
        keep_labels.append(line)
    
    im_width  = image.shape[1] 
    im_height =image.shape[0]
    for rr in r:
        if str(rr["cls"]) != "5" or str(rr["cls"]) != "6":
            keep_labels.append(" ".join([str(rr["cls"]), str(rr["xc"]), str(rr["yc"]), str(rr["w"]), str(rr["h"])]))
    with open(image_path.replace(".jpg", ".txt"), mode="w", encoding="utf-8") as f:
        try:
            f.write("\n".join(keep_labels))
        except:
            import ipdb; ipdb.set_trace()
            #print("hello" +"\n".join(keep_labels))
            return
if __name__=="__main__":
    for image in glob(os.path.join(os.getcwd(), "train_BED/")+"*.jpg"):
        request_micro_service(image)
    # with Pool(16) as p:
    #     x = tqdm(p.imap(request_micro_service, glob(os.path.join(os.getcwd(), "train_BED/")+"*.jpg") ))
    #     print(x)


