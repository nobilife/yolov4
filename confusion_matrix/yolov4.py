import logging
import subprocess
import time
import os
import cv2
from uuid import uuid4
from cv2 import data
# import pandas as pd
from glob import glob
from tqdm import tqdm
import re
import numpy as np
import base64
import cv2
import os
import sys
import json
import base64
import requests

idx_cls_dict = {0: "standing", 1: "sitting", 2: "sitting on bed", 3: "lying on bed",
                4: "lying on floor", 7: "no label", 5: "occupied_bed", 6: "empty_bed"}
cls_dict = {"standing": 0, "sitting": 1, "sitting on bed": 2, "lying on bed": 3,
            "lying on floor": 4, "no label": 7, "occupied_bed": 5, "empty_bed": 6}


route = "/yolov4/inference"
URL = "http://{}:{}{}".format("127.0.0.1", "2210", route)


class YoloV4():
    def __init__(self, darknet: str, config_file: str, data_file: str, weights_file: str):
        # Use the path to the darknet file I sent you
        self.darknet = darknet
        self.config_file = config_file
        self.data_file = data_file
        self.weights_file = weights_file
        self.first_time = True
       # self.thresh = "-"
        assert os.path.exists(self.config_file)
        assert os.path.exists(self.weights_file)
        self.pr = subprocess.Popen(
            [self.darknet, 'detector', 'test', self.data_file, self.config_file, self.weights_file, '-dont_show', '-ext_output', '-thresh', '0.6'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1, universal_newlines=True)
        print(self.pr)

    def stop(self):
        self.pr.stdin.close()

    def process(self, camera_images, _ceil=True):
        detected_objects = []
        for cam_idx, camera_image in enumerate(camera_images):
            detections = []
            image_path = self.__preprocess(camera_image)
            output = ""
            self.pr.stdin.write(image_path + "\n")
            while True:
                c = self.pr.stdout.read(1)
                output += c
                if "Enter Image Path" in output:
                    if self.first_time:
                        output = ""
                        self.first_time = False
                        continue
                    break
            result = output.split("\n")
            for i in range(5, len(result)):
                data = result[i-1]
                label = data.split(":")[0]
                confidient = int(data.split(":")[1].split("%")[0])
                bbox = data.split("(")[1].split(")")[0]
                if _ceil:
                    left_x = int(re.search('left_x:(.*)top_y', bbox).group(1))
                    left_x = 0 if left_x < 0 else left_x
                    top_y = int(re.search('top_y:(.*)width', bbox).group(1))
                    top_y = 0 if top_y < 0 else top_y
                    width = int(re.search('width:(.*)height', bbox).group(1))
                    height = int(re.search('height:(.*)', bbox).group(1))
                else:
                    left_x = float(
                        re.search('left_x:(.*)top_y', bbox).group(1))
                    left_x = 0. if left_x < 0 else left_x
                    top_y = float(re.search('top_y:(.*)width', bbox).group(1))
                    top_y = 0. if top_y < 0 else top_y
                    width = float(re.search('width:(.*)height', bbox).group(1))
                    height = float(re.search('height:(.*)', bbox).group(1))

                detections.append(
                    (label, confidient, (left_x, top_y, width, height), cam_idx))

            detected_objects = detected_objects + \
                self.__postprocess(camera_image, detections)
            os.remove(image_path)
        return detected_objects

    def __preprocess(self, camera_image):
        path = os.getcwd()
        image_path = path + "/" + uuid4().hex + ".jpg"
        cv2.imwrite(image_path, camera_image)
        return image_path

    def __postprocess(self, camera_image, detections):
        detected_objects = []
        for detection in detections:
            label, confidence, (x, y, w, h), cam_id = detection
            detected_objects.append([
                camera_image,       # CameraImage
                label,              # str
                (x, y, w, h),       # BoundingBox
                confidence,         # int
                cam_id
            ])
        return detected_objects


def m_convert_box(detected_objects, WIDTH, HEIGHT):
    return_boxes = []
    for d_o in detected_objects:
        _, label, (left_x, top_y, width, height), confidence, cam_id = d_o
        results = {}
        results.update({"cls": cls_dict[label]})
        results.update({"xc": float(left_x + width / 2.0) / WIDTH})
        results.update({"yc": float(top_y + height / 2.0) / HEIGHT})
        results.update({"w": float(width) / WIDTH})
        results.update({"h": float(height) / HEIGHT})
        #results.update({"confidence": confidence/100.})
        return_boxes.append(results)

    return return_boxes


if __name__ == "yolov4":
    def request_micro_service(image):
        _, np_img = cv2.imencode(".png", image)
        byte_img = np_img.tostring()
        base64_bytes = base64.b64encode(byte_img)
        response = requests.post(url=URL, data=base64_bytes, headers={
                                 'content-type': 'image/png'})
        if response.ok:
            r = response.json()
            return r
        else:
            return None

if __name__ == "__main__":
    from flask import Flask, request, Response

    def flask_micro_service():
        app = Flask(__name__)
        yolov4 = YoloV4(darknet="/home/ubuntu/re-train/darknet/darknet",
                        config_file="/home/ubuntu/re-train/darknet/nobi/scaled_nobi_pose_v3.cfg",
                        data_file="/home/ubuntu/re-train/darknet/nobi/scaled_nobi_pose_v3.data",
                        weights_file="/home/ubuntu/re-train/darknet/nobi/scaled_nobi_pose_v3.weights")

        @app.route(route, methods=['POST'])
        def m():
            r = request
            if r.method == "POST":
                image_data = base64.decodebytes(r.data)
                nparr = np.fromstring(image_data, np.uint8)
                img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

                if img is not None:
                    objects_predicted = yolov4.process([img])
                    response_pickled = json.dumps(m_convert_box(objects_predicted, WIDTH=img.shape[1], HEIGHT=img.shape[0]))
                else:
                    response = {"HNIW": None}
                    response_pickled = json.dumps(response)
            else:
                response = {"HNIW": None}
                response_pickled = json.dumps(response)

            return Response(response=response_pickled, status=200, mimetype="application/json")

        app.run(host="0.0.0.0", port="2210")


if __name__ == "__main__":
    flask_micro_service()

