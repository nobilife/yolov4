import imageio
import imgaug as ia
from glob import glob
from imgaug import augmenters as iaa
from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage
from uuid import uuid4
import cv2
from glob import glob
from multiprocessing import Process, Pool


mseq = iaa.Sequential([
    iaa.Fliplr(0.5), # horizontal flips
    #iaa.Sometimes(0.5, iaa.Crop(percent=(0, 0.1))),
    iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
    #iaa.Sometimes(0.5, iaa.Affine(scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}, translate_percent={"x": (-0.05, 0.05), "y": (-0.05, 0.05)}, rotate=(-10, 0), cval=(0, 255), mode="edge")),
    iaa.OneOf([iaa.GaussianBlur((0, 0.5)), iaa.AverageBlur(k=(2, 5)), iaa.MedianBlur(k=(3, 5))]),
    iaa.OneOf([iaa.Sometimes(0.1, iaa.Add((-50, 50), per_channel=0.5)), iaa.Sometimes(0.1, iaa.Multiply((0.6, 1.4), per_channel=0.5))]),
    ], random_order=True) # apply augmenters in random order


def label_to_box(file_path, im_height, im_width):
    labels = []
    #import ipdb ; ipdb.set_trace()
    with open(file_path, mode="r", encoding="utf-8") as f:
        data = f.read().strip().splitlines()
        for dd in data:
            ll = {}

            cls, xc, yc, w, h = dd.split(" ")
            cls = int(cls)
            x1 = int(im_width * (float(xc) - float(w) / 2))
            y1 = int(im_height * (float(yc) - float(h) / 2))
            x2 = int(im_width * (float(xc) + float(w) / 2))
            y2 = int(im_height * (float(yc) + float(h) / 2))

            ll.update({"cls": int(cls)})
            ll.update({"x1": int(im_width * (float(xc) - float(w) / 2))})
            ll.update({"y1": int(im_height * (float(yc) - float(h) / 2))})
            ll.update({"x2": int(im_width * (float(xc) + float(w) / 2))})
            ll.update({"y2": int(im_height * (float(yc) + float(h) / 2))})
            labels.append(ll)
    return labels


def box_to_label(prefix_path, image_aug, bbs_aug, im_height, im_width):
    file_path = prefix_path.replace(".jpg", f"_{uuid4().hex}")
    #import ipdb ; ipdb.set_trace()
    imageio.imwrite(f"{file_path}.jpg", image_aug)

    try:
        with open(f"{file_path}.txt", mode="w", encoding="utf-8") as f:
            for bb in bbs_aug:
                cls = bb.label
                xc = abs(float((bb.x1 + bb.x2) / (2 * im_width)))
                yc = abs(float((bb.y1 + bb.y2) / (2 * im_height)))
                w  = abs(float((bb.x1 - bb.x2) / im_width))
                h  = abs(float((bb.y1 - bb.y2) / im_height))
                f.write(" ".join([str(cls), str(xc), str(yc), str(w), str(h)]))
                f.write("\n")
        return True
    except Exception as e:
        print(e)
        return False

def main(fn):
    ia.seed(1)
   
    image = imageio.imread(fn)
    H, W, _ = image.shape
    bbs = BoundingBoxesOnImage([
        BoundingBox(x1=bb["x1"], x2=bb["x2"], y1=bb["y1"], y2=bb["y2"], label=bb["cls"]) for bb in label_to_box(fn.replace(".jpg", ".txt"), H, W)
    ], shape=image.shape)

    # ia.imshow(bbs.draw_on_image(image, size=2))
    # images_aug = seq(images)
    for i in range(6):
        image_aug, bbs_aug = mseq(image=image, bounding_boxes=bbs)
        #import ipdb ; ipdb.set_trace()
        box_to_label(fn, image_aug, bbs_aug, H, W)
        # ia.imshow(bbs_aug.draw_on_image(image_aug, size=2))

from tqdm import tqdm
with Pool(8) as p:
    for result in tqdm(p.imap(main, glob("/mnt/642C9F7E0555E58A/Nobi/addmoredata/augument/*.jpg"))):
        pass

# if __name__ == "__main__":
